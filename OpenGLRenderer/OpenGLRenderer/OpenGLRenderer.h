//
//  OpenGLRenderer.h
//  OpenGLRenderer
//
//  Created by Eryn Wells on 11/12/15.
//  Copyright © 2015 Eryn Wells. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for OpenGLRenderer.
FOUNDATION_EXPORT double OpenGLRendererVersionNumber;

//! Project version string for OpenGLRenderer.
FOUNDATION_EXPORT const unsigned char OpenGLRendererVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OpenGLRenderer/PublicHeader.h>


